package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
import ru.t1.aayakovlev.tm.service.dto.ProjectTaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.ProjectDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.dto.impl.ProjectTaskDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.dto.impl.TaskDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_TWO;

@Category(UnitCategory.class)
public final class ProjectTaskServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final ProjectDTOService projectService = new ProjectDTOServiceImpl(connectionService);

    @NotNull
    private static final TaskDTOService taskService = new TaskDTOServiceImpl(connectionService);

    @NotNull
    private final ProjectTaskDTOService service = new ProjectTaskDTOServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws AbstractException {
        projectService.save(PROJECT_USER_ONE);
        projectService.save(PROJECT_USER_TWO);
        taskService.save(TASK_USER_ONE);
        taskService.save(TASK_USER_TWO);
    }

    @After
    public void after() throws AbstractException {
        projectService.clear();
        taskService.clear();
    }

    @Test
    public void When_BindTaskToProject_Expect_TaskWithProjectId() throws AbstractException {
        projectService.save(PROJECT_ADMIN_ONE);
        taskService.save(TASK_EMPTY_PROJECT_ID);
        @NotNull final TaskDTO task = service.bindTaskToProject(
                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_EMPTY_PROJECT_ID.getId()
        );
        Assert.assertNotNull(task.getProjectId());
    }

    @Test
    public void When_UnbindTaskFromProject_Expect_TaskWithoutProjectId() throws AbstractException {
        projectService.save(PROJECT_ADMIN_ONE);
        taskService.save(TASK_ADMIN_ONE);
        @NotNull final TaskDTO task = service.unbindTaskFromProject(
                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_ADMIN_ONE.getId()
        );
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void When_RemoveProjectById_Expect_ThrowsEntityNotFoundException() throws AbstractException {
        projectService.save(PROJECT_ADMIN_TWO);
        taskService.save(TASK_ADMIN_ONE);
        taskService.save(TASK_ADMIN_TWO);
        Assert.assertTrue(taskService.findAllByProjectId(ADMIN_USER_TWO.getId(), PROJECT_ADMIN_TWO.getId()).isEmpty());
    }

}
