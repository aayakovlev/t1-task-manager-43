package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.impl.ProjectDTORepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.ProjectDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final ProjectDTORepository repository = new ProjectDTORepositoryImpl(connectionService.getEntityManager());

    @NotNull
    private static final ProjectDTOService service = new ProjectDTOServiceImpl(connectionService);

    @Before
    public void init() throws AbstractException {
        service.save(PROJECT_USER_ONE);
        service.save(PROJECT_USER_TWO);
    }

    @After
    public void after() throws AbstractException {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnProject() {
        @Nullable final ProjectDTO project = repository.findById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_USER_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_USER_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_USER_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_USER_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_USER_ONE.getCreated(), project.getCreated());
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnNull() {
        @Nullable final ProjectDTO project = repository.findById(USER_ID_NOT_EXISTED ,PROJECT_ID_NOT_EXISTED);
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullProject_Expect_ReturnProject() {
        repository.save(PROJECT_ADMIN_ONE);
        @Nullable final ProjectDTO project = repository.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_ADMIN_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getCreated(), project.getCreated());
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListProjects() {
        final List<ProjectDTO> projects = repository.findAll(COMMON_USER_ONE.getId());
        for (int i = 0; i < projects.size(); i++) {
            Assert.assertEquals(USER_PROJECT_LIST.get(i).getName(), projects.get(i).getName());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedProject_Expect_ReturnProject() {
        repository.save(PROJECT_ADMIN_TWO);
        repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveNotProject_Expect_ThrowsEntityNotFoundException() {
        repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId());
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_ZeroCountProjects() {
        repository.save(PROJECT_ADMIN_ONE);
        repository.save(PROJECT_ADMIN_TWO);
        repository.clear(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedProject_Expect_Project() {
        repository.save(PROJECT_ADMIN_TWO);
        repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId())
        );
    }

}
