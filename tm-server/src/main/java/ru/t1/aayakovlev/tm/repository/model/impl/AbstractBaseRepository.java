package ru.t1.aayakovlev.tm.repository.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.model.BaseRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractBaseRepository<E extends AbstractModel> implements BaseRepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractBaseRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected abstract Class<E> getClazz();

    @NotNull
    protected abstract String getSortColumnName(@NotNull final Comparator comparator);

    @Override
    public E save(@NotNull E entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public void clear() {
        @NotNull final String query = "delete from " + getClazz().getSimpleName();
        entityManager.createQuery(query).executeUpdate();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        @NotNull final String query = "from " + getClazz().getSimpleName();
        return entityManager.createQuery(query, getClazz())
                .getResultList();

    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " " +
                "order by " + getSortColumnName(comparator);
        return entityManager.createQuery(query, getClazz())
                .getResultList();
    }

    @Override
    @Nullable
    public E findById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Override
    public int count() {
        @NotNull final String query = "select count(*) from " + getClazz().getSimpleName();
        return entityManager.createQuery(query, Long.class)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.id = :id";
        entityManager.createQuery(query)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public E update(@NotNull final E entity) {
        entityManager.merge(entity);
        return entity;
    }

}
