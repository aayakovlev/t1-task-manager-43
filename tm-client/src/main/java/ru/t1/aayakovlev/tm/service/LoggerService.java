package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.Nullable;

public interface LoggerService {

    void info(@Nullable final String message);

    void command(@Nullable final String message);

    void debug(@Nullable final String message);

    void error(@Nullable final Exception e);

}
