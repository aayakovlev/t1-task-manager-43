package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.ProjectShowByIdRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    public static final String NAME = "project-show-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();

        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setId(id);

        @Nullable final ProjectDTO project = getProjectEndpoint().showProjectById(request).getProject();
        showProject(project);
    }

}
