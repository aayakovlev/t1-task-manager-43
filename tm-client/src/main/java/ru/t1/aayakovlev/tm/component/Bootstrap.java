package ru.t1.aayakovlev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.endpoint.*;
import ru.t1.aayakovlev.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.aayakovlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.aayakovlev.tm.repository.CommandRepository;
import ru.t1.aayakovlev.tm.repository.impl.CommandRepositoryImpl;
import ru.t1.aayakovlev.tm.service.*;
import ru.t1.aayakovlev.tm.service.impl.CommandServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.LoggerServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.TokenServiceImpl;
import ru.t1.aayakovlev.tm.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.*;
import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final CommandRepository commandRepository = new CommandRepositoryImpl();

    @Getter
    @NotNull
    private final CommandService commandService = new CommandServiceImpl(commandRepository);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final LoggerService loggerService = new LoggerServiceImpl();

    @Getter
    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @Getter
    @NotNull
    private final AuthEndpoint authEndpoint = AuthEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final SystemEndpoint systemEndpoint = SystemEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ProjectEndpoint projectEndpoint = ProjectEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final TokenService tokenService = new TokenServiceImpl();

    @Getter
    @NotNull
    private final TaskEndpoint taskEndpoint = TaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final UserEndpoint userEndpoint = UserEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final DomainEndpoint domainEndpoint = DomainEndpoint.newInstance(propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> commands = reflections.getSubTypesOf(AbstractCommand.class);
        commands.forEach(this::registry);
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    private void initPID() {
        @NotNull final String filename = PID_FILENAME;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
    }

    private boolean processArguments(@Nullable final String[] arguments) throws Exception {
        if (arguments == null || arguments.length == EMPTY_ARRAY_SIZE) return false;
        @Nullable final String argument = arguments[FIRST_ARRAY_ELEMENT_INDEX];
        return processArgument(argument);
    }

    private boolean processArgument(@Nullable final String argument) throws Exception {
        if (argument == null || argument.isEmpty()) return false;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
        return true;
    }

    public void processCommand(@Nullable final String command) throws Exception {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    public void prepareStartup() {
        initPID();
        loggerService.info(
                "___________   _____    _________  .__   .__                   __   \n" +
                "\\__    ___/  /     \\   \\_   ___ \\ |  |  |__|  ____    ____  _/  |_ \n" +
                "  |    |    /  \\ /  \\  /    \\  \\/ |  |  |  |_/ __ \\  /    \\ \\   __\\\n" +
                "  |    |   /    Y    \\ \\     \\____|  |__|  |\\  ___/ |   |  \\ |  |  \n" +
                "  |____|   \\____|__  /  \\______  /|____/|__| \\___  >|___|  / |__|  \n" +
                "                   \\/          \\/                \\/      \\/        ");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initFileScanner();
    }

    public void prepareShutdown() {
        fileScanner.stop();
        loggerService.info("*** APPLICATION SHUTTING DOWN ***");
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        try {
            @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
            registry(command);
        } catch (final @NotNull ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    private void registry(@NotNull AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) throws Exception {
        if (processArguments(args)) return;
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command: ");
                @Nullable final String command = nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
