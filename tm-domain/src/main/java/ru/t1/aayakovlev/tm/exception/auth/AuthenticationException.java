package ru.t1.aayakovlev.tm.exception.auth;

public final class AuthenticationException extends AbstractAuthException {

    public AuthenticationException() {
        super("Error! Failed to authenticate user with entered credentials...");
    }

}
