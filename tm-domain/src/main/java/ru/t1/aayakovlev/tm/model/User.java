package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user", schema = "public", catalog = "task_manager")
public final class User extends AbstractModel {

    private static final long serialVersionUID = 0;

    @Nullable
    @Column(name = "login", columnDefinition = "VARCHAR(64)")
    private String login;

    @Nullable
    @Column(name = "password", columnDefinition = "VARCHAR(32)")
    private String passwordHash;

    @Nullable
    @Column(name = "email", columnDefinition = "VARCHAR(64)")
    private String email;

    @Nullable
    @Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;

    @Nullable
    @Column(name = "last_name", columnDefinition = "VARCHAR(64)")
    private String lastName;

    @Nullable
    @Column(name = "middle_name", columnDefinition = "VARCHAR(64)")
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role", columnDefinition = "VARCHAR(64)")
    private Role role = Role.USUAL;

    @Column(name = "locked", columnDefinition = "BOOLEAN")
    private boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

    public User(@NotNull final String login, @NotNull final Role role, @NotNull String passwordHash) {
        this.login = login;
        this.role = role;
        this.passwordHash = passwordHash;
    }

}
